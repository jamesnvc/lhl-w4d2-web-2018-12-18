const pg = require('pg');
const conf = require('./config');

const client = new pg.Client(conf);

client.connect();

const albumByTitle = (title) => {
  return client.query(
    `select alb.id as id, alb.title as title, art.name as artist
     from albums alb
     join artists art on alb.artist_id = art.id
     -- don't actually need the cast to text here
     -- just for the sake of example
     where alb.title = $1::text;`,
    [title]);
};

const albumTracks = (albumId) => {
  return client.query(
    `select alb.title as album, art.name as artist,
            t.number, t.title
     from tracks t
     join albums alb on alb.id = t.album_id
     join artists art on alb.artist_id = art.id
     where t.album_id = $1::integer
     order by t.number asc;`,
    [albumId]);
};

module.exports = {
  albumByTitle,
  albumTracks,
  close: () => { client.end(); }
};
