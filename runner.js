const db = require('./db');

db.albumByTitle('Cowboy Bebop')
  .then((albums) => albums.rows[0].id)
  // .then((albumId) => db.albumTracks(albumId))
  .then(db.albumTracks)
  .then((tracks) => {
    console.log(tracks.rows);
    db.close();
    return true;
  })
  .catch((err) => {
    console.log("error", err);
    db.close();
  });

/*
db.albumByTitle('Cowboy Bebop', (err, albumResults) => {
  if (err) {
    console.log("error", err);
    db.close();
    return;
  }

  console.log(albumResults.rows);
  // exercise to the reader: check that there is only one result
  const albumId = albumResults.rows[0].id;

  db.albumTracks(albumId, (err, trackResults) => {
    if (err) {
      console.log("error", err);
    } else {
      console.log(trackResults.rows);
    }
    db.close();
  });

});

*/
